import React, { Fragment } from "react";
import parallax from "../assets/images/home_parallax.jpg";
import Particles from "react-particles-js";
const ParticleAnimation = () => {
  return (
    <Fragment>
      <div className="particles--container">
        <div className="overlay"></div>
        <Particles
          params={{
            detectRetina: true,
            fpsLimit: 60,
            interactivity: {
              detectsOn: "canvas",
              events: {
                onClick: {
                  enable: true,
                  mode: "push",
                },
                onHover: {
                  enable: true,
                  mode: "grab",
                  parallax: {
                    enable: true,
                    force: 60,
                    smooth: 10,
                  },
                },
                resize: true,
              },
              modes: {
                bubble: {
                  distance: 400,
                  duration: 2,
                  opacity: 0.8,
                  size: 40,
                },
                connect: {
                  distance: 80,
                  lineLinked: {
                    opacity: 0.5,
                  },
                  radius: 60,
                },
                grab: {
                  distance: 400,
                  lineLinked: {
                    opacity: 1,
                  },
                },
                push: {
                  quantity: 4,
                },
                remove: {
                  quantity: 2,
                },
                repulse: {
                  distance: 200,
                  duration: 0.4,
                  speed: 1,
                },
                slow: {
                  factor: 3,
                  radius: 200,
                },
              },
            },
            particles: {
              collisions: {
                enable: false,
                mode: "bounce",
              },
              color: {
                value: "#fbbb18",
              },
              lineLinked: {
                blink: false,
                color: {
                  value: "#fbbb18",
                },
                distance: 150,
                enable: true,
                opacity: 0.4,
                shadow: {
                  blur: 5,
                  color: {
                    value: "lime",
                  },
                  enable: false,
                },
                width: 1,
              },
              move: {
                attract: {
                  enable: false,
                  rotate: {
                    x: 600,
                    y: 1200,
                  },
                },
                direction: "none",
                enable: true,
                noise: {
                  factor: {
                    horizontal: {
                      value: 50,
                      offset: 0,
                    },
                    vertical: {
                      value: 10,
                      offset: 40000,
                    },
                  },
                },
                outMode: "out",
                random: false,
                speed: 2,
                straight: false,
                trail: {
                  enable: false,
                  length: 10,
                  fillColor: {
                    value: "#000000",
                  },
                },
              },
              number: {
                density: {
                  enable: true,
                  area: 800,
                  factor: 1000,
                },
                limit: 0,
                value: 100,
              },
              opacity: {
                animation: {
                  enable: true,
                  minimumValue: 0.1,
                  speed: 3,
                  sync: false,
                },
                random: {
                  enable: true,
                  minimumValue: 1,
                },
                value: 0.5,
              },

              shape: {
                options: {
                  character: {
                    fill: true,
                    close: true,
                    font: "Verdana",
                    style: "",
                    value: "*",
                    weight: "400",
                  },
                  char: {
                    fill: true,
                    close: true,
                    font: "Verdana",
                    style: "",
                    value: "*",
                    weight: "400",
                  },
                  image: {
                    fill: true,
                    close: true,
                    height: 100,
                    replaceColor: true,
                    src:
                      "https://cdn.matteobruni.it/images/particles/github.svg",
                    width: 100,
                  },
                  images: {
                    fill: true,
                    close: true,
                    height: 100,
                    replaceColor: true,
                    src:
                      "https://cdn.matteobruni.it/images/particles/github.svg",
                    width: 100,
                  },
                  polygon: {
                    fill: true,
                    close: true,
                    sides: 5,
                  },
                  star: {
                    fill: true,
                    close: true,
                    sides: 5,
                  },
                },
                type: "circle",
              },
              size: {
                animation: {
                  destroy: "none",
                  enable: true,
                  minimumValue: 0.1,
                  speed: 20,
                  startValue: "max",
                  sync: false,
                },
                random: {
                  enable: true,
                  minimumValue: 1,
                },
                value: 10,
              },
              stroke: {
                color: {
                  value: "#000000",
                },
                width: 0,
                opacity: 1,
              },
            },
            pauseOnBlur: true,
          }}
        />
      </div>

      <style jsx>
        {`
          .particles--container {
            position: relative;
            height: 40vh;
            width: 100%;
            background-image: url(${parallax});
            background-size: cover;
            background-position: center center;
            background-attachment: fixed;
          }
          .overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            background: black;
            opacity: 0.7;
          }
          :global(#tsparticles) {
            position: relative;
            height: 100%;
          }
        `}
      </style>
    </Fragment>
  );
};
export default ParticleAnimation;
