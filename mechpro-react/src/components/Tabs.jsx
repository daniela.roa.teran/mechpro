import React, { Fragment, useState } from "react";
const Tabs = ({ list, ...rest }) => {
  const [active, setActive] = useState("item0");
  return (
    <Fragment>
      <div className="tabs--container" {...rest}>
        <ul className="list--options">
          {list.map((item, i) => {
            return (
              <li key={`tab_op${i}`}>
                <label id={`item${i}`} onClick={(e) => setActive(e.target.id)}>
                  {item.title}
                </label>
              </li>
            );
          })}
        </ul>
        <div className="list--content">
          {list.map((item, i) => {
            return (
              <p key={`tab_content${i}`} content={`item${i}`}>
                {item.content}
              </p>
            );
          })}
        </div>
      </div>
      <style jsx>{`
        .tabs--container {
          display: flex;
          min-height: 30vh;
          flex-direction: column;
        }
        .list--options {
          padding: 0;
          list-style: none;
          display: flex;
          justify-content: center;
        }
        .list--options li {
          padding: 0 10px;
        }
        .list--options li label:hover{
            cursor: pointer
        }
        .list--options li label:after {
            content: "";
            display: flex;
            background-color: #0054a7;
            height: 3px;
            margin: 0 auto;
            margin-top: 5px;
            width: 0;
            transition: all 0.3s ease;
        }
        .list--content p {
          margin:0;
          display: flex;
          visibility: hidden;
          height: 0;
        }
        .list--content p[content=${active}] {
          visibility: visible;
          height: 100%;
        }
        .list--options li label[id=${active}]{
            font-family: notoBold;
            font-size: 13px;
            text-transform: uppercase;
            color: #0054a7;
            letter-spacing: 1.2px;
            text-align: center;
        }
        .list--options li label[id=${active}]:after, .list--options li label:hover:after{
            width: 60%;
        }
        @media screen and (max-width: 768px){
            .list--options {
                margin: 0;
                margin-right: 20px;
                flex-direction: column;
            }
            .list--options li label{
                padding-left: 10px;
            }
            .list--options li label[id=${active}]:after{
                width: 2px;
                display: flex;
                height: 10px;
                position: relative;
                margin: 0;
                bottom: 15px;
            }
            .list--content p {
                text-align: left!important;
            }
            .tabs--container {
                flex-direction: row;
                align-items: center;
            }
        }
      `}</style>
    </Fragment>
  );
};
export default Tabs;
