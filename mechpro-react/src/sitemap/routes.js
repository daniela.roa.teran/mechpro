import React from "react";
import { Route } from "react-router";

export default (
  <Route>
    <Route path="/" />
    <Route path="/sectores" />
    <Route path="/elegirnos" />
    <Route path="/contacto" />
  </Route>
);
