import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Router } from "react-router-dom";
import { LanguageProvider } from "./containers/LanguageContext";
import { createBrowserHistory } from "history";

const history = createBrowserHistory();

ReactDOM.render(
  <Router history={history}>
    <LanguageProvider>
      <App />
    </LanguageProvider>
  </Router>,
  document.getElementById("root")
);
serviceWorker.unregister();
