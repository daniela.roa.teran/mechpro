import React, { Fragment } from "react";
import {
  allies_images,
  whyus_parallax,
  company,
  personal,
} from "../assets/images/index";
const WhyUs = ({ fixture }) => {
  return (
    <Fragment>
      <div className="whyus--container container">
        <div className="whyus--content grey">
          <h1 className="title line--top">{fixture.whyus.title}</h1>
          <div className="whyus--list">
            {fixture.whyus.items.map((el, i) => {
              return (
                <>
                  <div className="whyus--item__img" data-aos="fade-up">
                    <img
                      src={el.image === "company" ? company : personal}
                      alt={`why us ${el.image} items`}
                    />
                    <ul
                      className="whyus--item info--list dark"
                      key={`item_content_${el.image}`}
                      data-aos="flip-left"
                      data-aos-delay="400"
                    >
                      {el.content.map((item, i) => {
                        return (
                          <Fragment key={`list_option_${i}`}>
                            <li>
                              <span>{item}</span>
                            </li>
                          </Fragment>
                        );
                      })}
                    </ul>
                  </div>
                </>
              );
            })}
          </div>
        </div>
        <div className="parallax"></div>
        <div className="allies--section">
          <h2 className="mini--title">{fixture.allies.title}</h2>
          <h2 className="title">{fixture.allies.subtitle}</h2>
          <div className="whyus--grid">
            <div>
              <img src={allies_images.item_1} alt="abb logo" />
            </div>
            <div>
              <img src={allies_images.item_2} alt="aeg logo" />
            </div>
            <div>
              <img src={allies_images.item_3} alt="buhler logo" />
            </div>
            <div>
              <img src={allies_images.item_4} alt="dicson logo" />
            </div>
            <div>
              <img src={allies_images.item_5} alt="dyprotec logo" />
            </div>
            <div>
              <img src={allies_images.item_6} alt="obralux logo" />
            </div>
            <div>
              <img src={allies_images.item_7} alt="rovema logo" />
            </div>
            <div>
              <img src={allies_images.item_8} alt="telemecanique" />
            </div>
          </div>
        </div>
      </div>

      <style jsx>
        {`
          .whyus--container {
            display: flex;
            flex-direction: column;
            margin-top: 80px;
            margin-bottom: 64px;
          }
          .fixed--container img {
            width: 100%;
            margin: 10vh 0;
          }
          .title {
            margin-bottom: 50px;
          }
          .whyus--content {
            padding: 40px;
          }
          .whyus--list {
            display: grid;
            grid-template-columns: 1fr;
            row-gap: 30px;
            align-items: stretch;
          }
          .whyus--item__img:nth-child(2) {
            flex-direction: row-reverse;
          }
          .whyus--item__img:nth-child(2) .whyus--item {
            left: 20px;
          }
          .whyus--item {
            justify-content: center;
            display: flex;
            flex-direction: column;
            margin: auto 0;
            list-style: none;
            padding: 0 30px;
            border: 1px solid #8d6708;
            border-radius: 4px;
            position: relative;
            left: -20px;
            background: #fafafa;
          }
          .info--list li:before {
            font-size: 9px;
          }
          .whyus--list ul li {
            padding: 5px 0;
            font-size: 13px;
          }
          .whyus--item__img {
            display: flex;
            flex-direction: row;
          }
          .whyus--item__img img {
            width: 40vw;
            height: 50vh;
            object-fit: cover;
          }
          .allies--section {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin: 3vw 7vw;
          }
          .allies--section .title {
            text-align: center;
          }
          .whyus--grid {
            display: grid;
            grid-template-columns: repeat(4, 1fr);
            align-items: center;
            justify-items: center;
            grid-column-gap: 3vw;
            margin: 0 5vw;
          }
          .whyus--grid img {
            max-width: 150px;
            width: 100%;
            object-fit: contain;
            filter: grayscale(1);
            transition: all 0.3s ease;
          }
          .whyus--grid div {
            align-items: center;
            display: flex;
          }
          .whyus--grid div:hover img {
            filter: none;
            transform: scale(1.2);
          }
          .whyus--container .parallax {
            background-image: url(${whyus_parallax});
          }
          @media screen and (max-width: 768px) {
            .whyus--content {
              grid-template-columns: 1fr;
              grid-row-gap: 30px;
              margin: 0;
              padding: 35px;
            }
          }
          @media screen and (max-width: 495px) {
            .whyus--list {
              grid-template-columns: 100%;
            }
            .whyus--item__img,
            .whyus--item__img:nth-child(2) {
              flex-direction: column;
            }
            .whyus--item__img img {
              width: 100%;
              height: 40vh;
              object-fit: cover;
            }
            .whyus--item {
              top: -50px;
            }
          }
        `}
      </style>
    </Fragment>
  );
};
export default WhyUs;
