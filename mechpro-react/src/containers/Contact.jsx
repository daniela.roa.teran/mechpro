import React, { Fragment } from "react";
import { background_contact } from "../assets/images/index";
const Contact = ({ fixture, link }) => {
  return (
    <Fragment>
      <div className="contact--container">
        <div className="contact--wrap">
          <div className="contact--content">
            <h1 className="title line--top dark">{fixture.title}</h1>
            <p>{fixture.content}</p>
          </div>
          <div className="contact--info">
            <ul>
              <li>
                <h2>{fixture.web}</h2>
                <span>www.mechproltd.com</span>
              </li>
              <li>
                <h2>{fixture.email}</h2>
                <span>mechpro.ltd@gmail.com</span>
                <span>mechpro@mechproltd.com</span>
                <span>sales@mechproltd.com</span>
              </li>
              <li>
                <h2>{fixture.phone}</h2>
                <span>(1) 646 707 7313</span>
                <span>(58) 412 293 5766</span>
                <span>(58) 212 331 3287</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <style jsx>{`
        .contact--container {
          display: flex;
          justify-content: center;
          align-items: center;
          padding-top: 80px;
          height: calc(100vh - 144px);
          background-image: url(${background_contact});
          background-position: center center;
          background-size: cover;
          background-repeat: no-repeat;
          color: #fafafa;
        }
        .contact--content .mini--title {
          color: #dba419;
        }
        .contact--wrap {
          display: grid;
          grid-template-columns: repeat(2, 1fr);
          align-items: center;
          height: 100%;
          background: rgba(0, 0, 0, 0.8);
          padding: 0 25px;
        }
        .contact--content {
          display: flex;
          flex-direction: column;
          align-items: flex-start;
        }
        .contact--content .title {
          margin: 15px 0;
        }
        .contact--info {
          display: flex;
          justify-content: center;
        }
        .contact--info ul {
          list-style: none;
          padding: 0;
          margin: 0;
          display: flex;
          flex-direction: column;
        }
        .contact--info ul li {
          display: flex;
          flex-direction: column;
        }
        .contact--info ul li span {
          display: flex;
          flex-direction: row;
          align-items: center;
        }
        .contact--info ul li span:before {
          content: "";
          display: flex;
          width: 8px;
          height: 8px;
          margin-right: 5px;
          background-color: #dba419;
          transform: rotate(45deg);
          position: relative;
          top: 2px;
        }
        @media screen and (max-width: 650px) {
          .contact--info {
            justify-content: flex-start;
          }
          .contact--container {
            height: 100%;
            min-height: calc(100vh - 134px);
            flex-grow: 1;
            flex-direction: column;
            padding-top: 80px;
          }
          .contact--wrap {
            display: flex;
            flex-direction: column;
            align-items: baseline;
            flex-grow: 1;
            padding: 25px;
            padding-top: 0;
            justify-content: center;
          }
          .contact--info h2 {
            margin: 10px 0;
          }
          .contact--content p {
            margin: 0;
          }
        }
        @media screen and (min-width: 650px) and (max-height: 495px) {
          .contact--wrap {
            grid-template-columns: 1fr;
            width: 100%;
          }
          .contact--info {
            justify-content: flex-start;
          }

          .contact--container {
            height: 100%;
          }
          .contact--wrap {
            padding: 25px;
          }
        }
      `}</style>
    </Fragment>
  );
};
export default Contact;
