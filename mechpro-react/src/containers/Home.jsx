import React, { Fragment, useEffect, useState } from "react";
import AOS from "aos";
import { useHistory } from "react-router-dom";
import Tabs from "../components/Tabs";
import * as media from "../assets/images/index";
import ParticleAnimation from "../components/ParticleAnimation";
import Accordion from "../components/Accordion";
import Carousel from "react-elastic-carousel";
import Media from "react-media";

const Home = ({ fixture, link }) => {
  const [isVideoLoaded, setVideoLoaded] = useState(false);
  useEffect(() => {
    AOS.init({
      duration: 2000,
    });
  });
  const onLoadedVideo = () => {
    setVideoLoaded(true);
  };
  let history = useHistory();
  return (
    <Fragment>
      <div className="banner--container">
        <img
          src={media.home_video_thumb}
          alt="video thumb"
          title="video image thumbnail"
          style={{ opacity: isVideoLoaded ? 0 : 1 }}
        />

        <video
          autoPlay
          loop
          muted
          src={media.banner_video}
          id="homeVid"
          onLoadedData={onLoadedVideo}
          style={{ opacity: isVideoLoaded ? 1 : 0 }}
        ></video>
        <span className="mdi mdi-arrow-down"></span>
      </div>
      <div className="container">
        <div className="home--section__about content">
          <img
            className="about--background"
            loading="lazy"
            src={media.engineering}
            alt="engineering"
            title="engineering"
          />
          <article>
            <h2 className="mini--title" data-aos="fade-up">
              {fixture.mision.title}
            </h2>
            <h1 data-aos="fade-up" className="title">
              {fixture.mision.subtitle}
            </h1>
            <p data-aos="fade-up">{fixture.mision.content}</p>
          </article>
          <article>
            <h2 className="mini--title" data-aos="fade-up">
              {fixture.vision.title}
            </h2>
            <h1 className="title" data-aos="fade-up">
              {fixture.vision.subtitle}
            </h1>
            <p data-aos="fade-up">{fixture.vision.content}</p>
          </article>
        </div>
        <div className="home--section__about content"></div>

        <ParticleAnimation />

        <div className="home--section__about grey content">
          <article>
            <h2 className="mini--title" data-aos="fade-up">
              {fixture.objective.title}
            </h2>
            <h1 className="title" data-aos="fade-up">
              {fixture.objective.subtitle}
            </h1>
            <Media query={{ minWidth: 450 }}>
              {(matches) =>
                matches ? (
                  <Tabs
                    data-aos="fade-up"
                    list={fixture.objective.objectives}
                  />
                ) : (
                  fixture.objective.objectives.map((el, i) => {
                    return (
                      <Accordion
                        data-aos="fade-up"
                        key={`accordion_el${i}`}
                        title={el.title}
                      >
                        <p>{el.content}</p>
                      </Accordion>
                    );
                  })
                )
              }
            </Media>
          </article>
        </div>
        <div className="home--section__contact">
          <div className="content">
            <h1 data-aos="fade-right">{fixture.contact.content}</h1>
            <button
              onClick={() => history.push(fixture.contact.link.toLowerCase())}
              className="button button--link gold"
            >
              {fixture.contact.contact_button}
            </button>
          </div>
          <div className="image">
            <img
              loading="lazy"
              src={media.home_contact}
              alt="contact"
              title="contact from home"
            />
          </div>
        </div>
      </div>
      <div className="home--section__values grey">
        <div className="values--content">
          <h2 className="mini--title" data-aos="fade-up">
            {fixture.value.title}
          </h2>
          <h1 className="title" data-aos="fade-up">
            {fixture.value.subtitle}
          </h1>
          <Carousel
            itemsToShow={4}
            enableAutoPlay={true}
            breakPoints={[
              { width: 1, itemsToShow: 1 },
              { width: 320, itemsToShow: 2 },
              { width: 495, itemsToShow: 2 },
              { width: 560, itemsToShow: 3 },
              { width: 768, itemsToShow: 4 },
              { width: 1024, itemsToShow: 4 },
            ]}
          >
            {fixture.value.values.map((el, i) => {
              return (
                <div key={`values_item${i}`} className="values--item">
                  <div className="hexagon">
                    <img
                      title="valores compromiso"
                      loading="lazy"
                      alt="icono compromiso"
                      src={media.values_images[el.id]}
                    />
                  </div>
                  <span>{el.title}</span>
                </div>
              );
            })}
          </Carousel>
        </div>
      </div>
      <style jsx>
        {`
          .banner--container video {
            width: 100%;
            height: 100vh;
            object-fit: cover;
            filter: brightness(0.8);
            opacity: ${isVideoLoaded ? 1 : 0};
            transition: opacity 1s ease;
          }
          .banner--container img{
            width: 100%;
            position: absolute;
            height: 100%;
            object-fit: cover;
            opacity: ${isVideoLoaded ? 0 : 1};
            z-index: ${isVideoLoaded ? -10 : 2};
            filter: brightness(0.8);
            transition: opacity 1s ease;
          }
          .banner--container {
            display: flex;
          }
          .banner--container span.mdi {
              cursor:pointer;
              height: 60px;
              width: 80px;
              margin: 0px 0 0 -40px;
              line-height: 60px;
              position: absolute;
              left: 50%;
              bottom: 0px;
              color: #FFF;
              text-align: center;
              font-size: 70px;
              z-index: 2;
              text-decoration: none;
              text-shadow: 0px 0px 3px rgba(0, 0, 0, 0.4);
              animation: ca3_fade_move_down 2s ease-in-out infinite;
            }
            @-webkit-keyframes ca3_fade_move_down {
              0%   { -webkit-transform:translate(0,-20px); opacity: 0;  }
              50%  { opacity: 1;  }
              100% { -webkit-transform:translate(0,20px); opacity: 0; }
            }
            @-moz-keyframes ca3_fade_move_down {
              0%   { -moz-transform:translate(0,-20px); opacity: 0;  }
              50%  { opacity: 1;  }
              100% { -moz-transform:translate(0,20px); opacity: 0; }
            }
            @keyframes ca3_fade_move_down {
              0%   { transform:translate(0,-20px); opacity: 0;  }
              50%  { opacity: 1;  }
              100% { transform:translate(0,20px); opacity: 0; }
            }
          }
          .home--section__about {
            position: relative;
            background-color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 2;
            overflow: hidden;
            flex-direction: column;
          }
          .home--section__about article:nth-of-type(2) {
            padding-top: 0;
          }
          .home--section__about .about--background {
            width: 100%;
            height: 100%;
            filter: grayscale(1);
            position: absolute;
            opacity: 0.1;
            display: flex;
            object-fit: cover;
          }
          .home--section__about article {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            padding: 3vw 7vw;
          }

          .home--section__about article p {
            text-align: center;
            letter-spacing: 1.2px;
            line-height: 30px;
          }
          .home--section__about .accordion-content p {
            text-align: left;
          }
          article {
            max-width: 780px;
            margin: 0 auto;
            display: flex;
            justify-content: center;
          }
          .home--section__contact {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
          }
          .home--section__contact .title {
            text-align: center;
          }
          .home--section__contact .image {
            filter: brightness(0.7);
          }
          .home--section__contact .content {
            padding: 0 10px;
            background-color: #212121;
            color: #fafafa;
            text-align: center;
          }
          .home--section__contact .image,
          .home--section__contact .content {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }
          .home--section__contact .image img {
            height: 100%;
            width: 100%;
            object-fit: cover;
          }
          .values--content {
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 0 7vw;
          }
          .values--item {
            display: flex;
            flex-direction: column;
            align-items: center;
          }
          .values--item span {
            text-align: center;
          }
          .values--item span:first-letter {
            text-transform: capitalize;
          }
          .values--item img {
            height: 100%;
            object-fit: contain;
            filter: opacity(0.7);
          }

          .home--section__values {
            padding: 20px 0;
          }
          .home--section__values h2 {
            margin-top: 0;
          }
          @media screen and (max-width: 768px) {
            .values--content ul li {
              flex: 2 0 50%;
            }
            .home--section__contact {
              grid-template-columns: 1fr;
              grid-template-rows: repeat(2, 1fr);
            }
          }
          @media screen and (max-width: 576px) {
            .values--content ul li {
              flex: 1 0 100%;
            }
            .values--content {
              padding: 0;
            }
            .home--section__about h1.title {
              text-align: center;
            }
            .home--section__about {
              top: -90px;
            }
          }
        `}
      </style>
    </Fragment>
  );
};
export default Home;
