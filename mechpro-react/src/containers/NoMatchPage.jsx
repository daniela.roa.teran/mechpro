import React, { useContext } from "react";
import { LanguageContext } from "../containers/LanguageContext";
import { no_match } from "../assets/images/index";
import Particles from "react-particles-js";
import { useHistory } from "react-router-dom";

const NoMatchPage = () => {
  const { state } = useContext(LanguageContext);
  let history = useHistory();
  return (
    <>
      <div className="nomatch--container">
        <Particles
          params={{
            particles: {
              number: {
                value: 355,
                density: {
                  enable: true,
                  value_area: 789.1476416322727,
                },
              },
              color: {
                value: "#ffffff",
              },
              shape: {
                type: "circle",
                stroke: {
                  width: 0,
                  color: "#000000",
                },
                polygon: {
                  nb_sides: 5,
                },
                image: {
                  src: "img/github.svg",
                  width: 100,
                  height: 100,
                },
              },
              opacity: {
                value: 0.48927153781200905,
                random: false,
                anim: {
                  enable: true,
                  speed: 0.2,
                  opacity_min: 0,
                  sync: false,
                },
              },
              size: {
                value: 2,
                random: true,
                anim: {
                  enable: true,
                  speed: 2,
                  size_min: 0,
                  sync: false,
                },
              },
              line_linked: {
                enable: false,
                distance: 150,
                color: "#ffffff",
                opacity: 0.4,
                width: 1,
              },
              move: {
                enable: true,
                speed: 0.2,
                direction: "none",
                random: true,
                straight: false,
                out_mode: "out",
                bounce: false,
                attract: {
                  enable: false,
                  rotateX: 600,
                  rotateY: 1200,
                },
              },
            },
            interactivity: {
              detect_on: "canvas",
              events: {
                onhover: {
                  enable: true,
                  mode: "bubble",
                },
                onclick: {
                  enable: true,
                  mode: "push",
                },
                resize: true,
              },
              modes: {
                grab: {
                  distance: 400,
                  line_linked: {
                    opacity: 1,
                  },
                },
                bubble: {
                  distance: 83.91608391608392,
                  size: 1,
                  duration: 3,
                  opacity: 1,
                  speed: 3,
                },
                repulse: {
                  distance: 200,
                  duration: 0.4,
                },
                push: {
                  particles_nb: 4,
                },
                remove: {
                  particles_nb: 2,
                },
              },
            },
            retina_detect: true,
          }}
        />
        <h1>404</h1>
        <h2>
          {state.lang === "ES" ? "Página no encontrada" : "Page not found"}
        </h2>
        <button
          className="button button--link"
          onClick={() => history.push("/")}
        >
          {state.lang === "ES" ? "Página principal" : "Homapage"}
        </button>
      </div>
      <style jsx>
        {`
          :global(.navbar--fixed) {
            display: none;
          }
          :global(body) {
            background: url(${no_match}) no-repeat center center fixed;
            background-size: cover;
            background-color: #fff;
            height: 100%;
            color: #fafafa;
          }
          .nomatch--container {
            display: flex;
            flex-direction: column;
            flex-grow: 1;
            justify-content: center;
            align-items: center;
            position: relative;
            text-align: center;
          }
          :global(#tsparticles) {
            display: flex;
            position: absolute;
            width: 100%;
            height: 100%;
          }

          h1 {
            font-size: 9em;
            margin: 5px 0;
          }
          h2 {
            font-size: 2.5em;
            margin: 0;
            text-transform: uppercase;
          }
          @media screen and (max-width: 768px) {
            :global(.app--main__content) {
              min-height: calc(100vh - 58px);
            }
            h1 {
              font-size: 7em;
            }

            h2 {
              font-size: 1.5em;
            }
          }
        `}
      </style>
    </>
  );
};
export default NoMatchPage;
