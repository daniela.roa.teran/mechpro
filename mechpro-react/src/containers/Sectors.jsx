import React, { Fragment, useState } from "react";
import Media from "react-media";
import { StickyContainer, Sticky } from "react-sticky";
import { sectors } from "../assets/images/index";

const Sectors = ({ fixture, fixture_mobile }) => {
  const [active, setActive] = useState("item_1");
  const sector_image = sectors[active];
  return (
    <Fragment>
      <div className="sectors--container container">
        <div className="sectors--content">
          <Media query={{ minWidth: 768 }}>
            {(matches) =>
              matches ? (
                <Fragment>
                  <div className="sectors--options">
                    <div className="overlay"></div>
                    <ul className="info--list">
                      {Object.keys(fixture.menu).map((item, i) => {
                        return (
                          <Fragment key={`option_${i}`}>
                            <li>
                              <h3
                                id={fixture.menu[item].id}
                                data-elem={fixture.menu[item].id}
                                onClick={(e) => {
                                  setActive(e.currentTarget.dataset.elem);
                                }}
                              >
                                {fixture.menu[item].title}
                              </h3>
                            </li>
                          </Fragment>
                        );
                      })}
                    </ul>
                  </div>
                  <div className="sectors--info">
                    {Object.keys(fixture.content).map((item, i) => {
                      return (
                        <Fragment key={`sector_info_${i}`}>
                          <ul
                            className="info--list"
                            content={fixture.content[item].id}
                          >
                            {Object.keys(fixture.content[item].content).map(
                              (subitem, i) => {
                                return (
                                  <Fragment key={`sector_info_title_${i}`}>
                                    <h3>
                                      {
                                        fixture.content[item].content[subitem]
                                          .title
                                      }
                                    </h3>
                                    {fixture.content[item].content[
                                      subitem
                                    ].content.map((li, i) => {
                                      return (
                                        <li
                                          key={`${fixture.menu[item].id}_${i}`}
                                        >
                                          {li}
                                        </li>
                                      );
                                    })}
                                  </Fragment>
                                );
                              }
                            )}
                          </ul>
                        </Fragment>
                      );
                    })}
                  </div>
                </Fragment>
              ) : (
                <Fragment>
                  <div className="sectors--container__mobile">
                    {Object.keys(fixture_mobile.content).map((item, i) => {
                      return (
                        <StickyContainer key={`sector_mobile_title${i}`}>
                          <Sticky>
                            {({ style, isSticky }) => {
                              const newStyle = Object.assign(
                                {
                                  backgroundImage: `url(${
                                    sectors[fixture_mobile.content[item].id]
                                  })`,
                                },
                                style
                              );

                              return (
                                <div
                                  id={fixture_mobile.content[item].id}
                                  className={`sector--title ${
                                    isSticky ? "sticky" : ""
                                  }`}
                                  style={newStyle}
                                >
                                  <h2>{fixture_mobile.content[item].title}</h2>
                                </div>
                              );
                            }}
                          </Sticky>
                          <ul className="info--list">
                            {Object.keys(
                              fixture_mobile.content[item].content
                            ).map((subitem) => {
                              return (
                                <Fragment key={`sector_info_mobile_${i}`}>
                                  <h3>
                                    {
                                      fixture_mobile.content[item].content[
                                        subitem
                                      ].title
                                    }
                                  </h3>
                                  {fixture_mobile.content[item].content[
                                    subitem
                                  ].content.map((li, i) => {
                                    return (
                                      <li
                                        key={`${fixture_mobile.content[item].id}_${i}`}
                                      >
                                        {li}
                                      </li>
                                    );
                                  })}
                                </Fragment>
                              );
                            })}
                          </ul>
                        </StickyContainer>
                      );
                    })}
                  </div>
                </Fragment>
              )
            }
          </Media>
        </div>
      </div>
      <style jsx>{`
        .sectors--container {
          height: calc(100vh - 144px);
          padding-top: 80px;
          overflow: hidden;
          width: 100%;
        }
        .sectors--content {
          display: grid;
          height: 100%;
          grid-template-columns: repeat(2, 1fr);
        }
        .sectors--options {
          background-image: url(${sector_image});
          background-size: cover;
          display: flex;
          flex-direction: row;
          position: relative;
        }
        .sectors--options .overlay {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background: black;
          opacity: 0.4;
        }
        .sectors--options ul {
          z-index: 2;
          font-size: 25px;
          color: white;
          flex-grow: 1;
          justify-content: center;
          display: flex;
          flex-direction: column;
          list-style-type: "♦";
          margin-left: 10px;
          align-items: baseline;
          padding-left: 25px;
        }
        .sectors--options ul li {
          cursor: pointer;
          margin: 5px 0;
        }
        .sectors--options:after {
          content: "";
          height: auto;
          width: 85px;
          display: flex;
          transform: skew(-5deg, 0deg);
          background: #06192c;
          bottom: 0;
          left: 40px;
          position: relative;
        }
        .sectors--info {
          z-index: 2;
          background: #06192c;
          padding: 0;
          padding-right: 30px;
          color: white;
          display: flex;
          margin-left: 0;
          flex-direction: column;
          justify-content: center;
        }
        .sectors--info ul {
          overflow-y: scroll;
          height: 0px;
          align-items: baseline;
          display: flex;
          flex-direction: column;
          max-height: 70vh;
          width: 96%;    
          margin:0;
        }
        .sectors--info ul::-webkit-scrollbar {
          width: 5px;
        }
        .sectors--info ul::-webkit-scrollbar-track {
          box-shadow: inset 0 0 5px grey;
          border-radius: 10px;
        }
        .sectors--info ul::-webkit-scrollbar-thumb {
          background: #dfb144;
          border-radius: 10px;
        }
        .sectors--info ul[content="${active}"]{
          height: 100%;
          padding: 0 0 15px 25px;
          margin: 0;
          list-style: none;
        }
        .sectors--options li h3{
          margin: 0;
          transition: all 0.4s ease;
        }
        .sectors--options li h3[id="${active}"] {
          margin-left: 15px;
          font-size: 50px;
          position: relative;
          padding: 5px 20px;
        }
        .sectors--options li h3[id="${active}"]:before, .sectors--options li h3[id="${active}"]:after{
          content: "";
          display: block;
          position: absolute;
          width: 20%;
          height: 20%;
          border: 2px solid;
          border-radius: 2px;
        }
        .sectors--options li h3[id="${active}"]:before {
          top: 0;
          left: 0;
          border-top-color: #dba419;
          border-left-color: #dba419;
          border-bottom-color: transparent;
          border-right-color: transparent;
          transform: rotate(0);
        }
        .sectors--options li h3[id="${active}"]:after {
          bottom: 0;
          right: 0;
          border-top-color: transparent;
          border-left-color: transparent;
          border-bottom-color: #dba419;
          border-right-color: #dba419;
        }
        .sectors--container__mobile {
          display: flex;
          flex-direction: column;
        }
        .sectors--container__mobile h2{
          position: relative;
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 25px;
          background: rgba(0, 0, 0, 0.3);
        }
        .sectors--container__mobile .sector--title {
          background-position: center!important;
          background-repeat: no-repeat!important;
          background-attachment: fixed!important;
          background-size: cover!important;
          display: flex;
          width: 100%;
          margin: 0;
          color: white;
          background: rgba(0, 0, 0, 0.2);
          z-index: 23;
        }
        
        .sectors--container__mobile .sector--title.sticky, 
        .sectors--container__mobile .sector--title[id="item_1"]  {
          background-attachment: unset!important;
        }
        .sectors--container__mobile .info--list {
          padding: 0 30px 20px 40px;
        }
        @media screen and (max-width: 768px) {
          .sectors--content {
            grid-template-columns: 1fr;
            background-color: #fafafa;
          }
          .sectors--container {
            height: 100%;
            padding-top: 0;
          }
        }
        @media screen and (min-width: 300px) and (max-height: 495px) {
          .sectors--container {
            height: 100%;
          }
        }
      `}</style>
    </Fragment>
  );
};
export default Sectors;
